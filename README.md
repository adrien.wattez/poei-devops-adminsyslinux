# POEI Administrateur Linux

## Présentation de ce que c'est un administrateur linux

De wikipedia. 
Les attributions classiques de l'administrateur sont les suivantes :
- l'installation et la désinstallation ;
- le paramétrage ;
- le maintien ;
- la mise à jour ;
- l'évolution ;
- la sauvegarde ;
- la restauration ;
- la planification ;
- la supervision ;
- le conseil ;
- le support ;
- la veille technologique dans le périmètre technique des matériels et logiciels de type serveur, principalement les systèmes d'exploitation. 
Il a parfois la tâche de l'administration du réseau et/ou de l'administration des bases de données dans des organisations de petite taille.

**En gros c'est l'homme/la femme à tout faire au sein d'un SI**


## Structure linux

FHS (Filesystem Hierarchy Standard Linux) :

```sh
/(root)
/bin
/sbin
/var
/usr
/tmp
/mnt
/media
/etc
```

https://fr.wikipedia.org/wiki/Filesystem_Hierarchy_Standard



## Manipuler des répertoires, lister les fichiers et lister les commandes

- `history` et `.bash_history` dans le home
- `PATH` env variable
- `export`une variable de shell peut être exportée pour devenir une variable d'environnement grâce à la commande `export.
- `man`
- `/usr/share/doc/` ou `/usr/share/man/` sont les répertoires où sont stockés les documentations et les `man` des commandes/binaires
- `.` et `..` -> `ls -la` permet d'afficher les permissions et propriétaires du répertoire courant `.` et parent `..`
- `home` et `~` attention à ne pas confondre le home du root quand vous êtes connecté en root (home -> /root) avec le home d'un autre utilisateur ( /!\ quand vous décidez de faire `sudo` et `~` dans la même commande)
- `ls -la ~` permet également d'afficher les fichiers cachés, ici dans le home
- `find / -name "*cequejecherche*"` retournera tous les fichiers dont le nom contient le mot `cequejecherche` à partir de la `/` /!\ cette commande peut prendre bcp de temps en fonction du contenu du serveur

## Gestion des utilisateurs

- `passwd` elle sert à gérer les mots de passe (password) des utilisateurs de votre système (pas seulement le vôtre).
- `usermod` modifier un compte utilisateur, ajout de groupe etc.
- Le fichier `/etc/group` est un fichier ASCII qui définit les groupes auxquels appartient chaque utilisateur.
- Le fameux groupe `wheel` qui est un groupe d'administrateur (cf. fichier `sudoers` qui gère les accès `sudo`):

```sh
$ sudo cat /etc/sudoers
...
## Allows people in group wheel to run all commands
%wheel ALL=(ALL)  ALL
...

```

> `/var/log/secure` contient les tentatives mais aussi les logs d'exécution de la commande sudo
> `Oct 22 22:40:43 centos-01 sudo: zenika : TTY=pts/0 ; PWD=/home/zenika ; USER=root ; COMMAND=/bin/cat /etc/passwd`

- le fichier `/etc/passwd` contient toutes les informations relatives aux utilisateurs (également les utilisateurs applicatifs qui ne peuvent pas se connecter à distance cf `nologin` pour apache)

```sh
$ sudo cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
zenika:x:1000:1000:zenika:/home/zenika:/bin/bash
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
...
```

```sh
$ sudo chown apache:apache ./*
// ou
$ sudo chown -R apache:apache ./
// ce n'est pas pareil
$ chown --help
...
-R --recursive opérer récursivement sur les fichiers et répertoires
...
```

> `R` ou `r` sont souvent pour exprimer le côté récursif de la fonction, /!\ pour grep `r` et `R` sont différents, `R` va suivre les liens symboliques

## SELinux (Security Enhanced Linux) le mécanisme de sécurité développé pour Linux

- `ls -Z` l’option `-Z` me permet d’afficher le contexte de sécurité de ces fichiers
 Un contexte SELinux est présenté sous la forme `utilisateur:rôle:type:niveau.

> https://www.microlinux.fr/selinux/`

```sh
$ ls -Z /etc/httpd
drwxr-xr-x. root root system_u:object_r:httpd_config_t:s0 conf
drwxr-xr-x. root root system_u:object_r:httpd_config_t:s0 conf.d
drwxr-xr-x. root root system_u:object_r:httpd_config_t:s0 conf.modules.d
lrwxrwxrwx. root root system_u:object_r:httpd_log_t:s0 logs -> ../../var/log/httpd
lrwxrwxrwx. root root system_u:object_r:httpd_modules_t:s0 modules -> ../../usr/lib64/httpd/modules
lrwxrwxrwx. root root system_u:object_r:httpd_config_t:s0 run -> /run/httpd
```

## La gestion de scripts et d'exécution des programmes

- `#! (shebang)`
- `/bin/bash`
- `echo`
- `Exit status` -> un `echo $?` permet d'afficher le retour de la dernière exécution

```sh
$ echo "zenika_power"
zenika_power
$ echo $?
0
$ toto
-bash: toto : commande introuvable
$ echo $?
127 // Ici c'est le retour d'un pb d'execution : commande introuvable
$ echo $?
0 // La commande précédente echo s'est correctement exécutée
```

## Rechercher un fichier et rechercher du texte dans des fichiers

- `find` et `grep` pour rechercher dans un fichier
- `grep -iRl "noindex" /etc/httpd/` permet de rechercher le texte "noindex" dans l'ensemble des fichiers de manière récursive dans le répertoire `/etc/httpd/`

## Installation/désinstallation de package

Les dépôts ou archives de téléchargement, ce sont les endroits (sites Internet ou locaux) à partir desquels Yum télécharge les logiciels et leurs mises à jour.
- `yum` vs `apt-get`

> `yum update` != `apt-get update

> `clean` permet d'effacer diverses choses qui s'accumulent dans le répertoire cache de yum au fil du temps.

Ces fichiers `*.repo` se trouvent dans le répertoire `/etc/yum.repos.d`
Dans sa configuration par défaut, la distribution CentOS offre un nombre relativement réduit de paquetages voire obsolètes cf. MAJ PHP 5 -> PHP 7 et MariaDB 5 -> MariaDB 10
Pour inclure un dépôt temporairement (cf. TP MAJ PHP 5):
```sh
# yum --enablerepo=<nom du dépôt> update
```


## Systemd

- `systemctl start <nom du service>.service`
- `systemctl stop <nom du service>.service`
- `systemctl restart <nom du service>.service` réalise un stop et un start (interruption de service)

- `systemctl reload <nom du service>.service` permet de recharger la configuration sans interrompre le service

- `systemctl enable <nom du service>.service` configure un service pour qu'il soit lancé automatiquement au démarrage du système
- `systemctl disable <nom du service>.service`


> TP : https://doc.ubuntu-fr.org/creer_un_service_avec_systemd

Superviser, investiguer:

- `systemctl status`
- `journalctl` avec Systemd comme init system sur une grande majorité des distributions Linux les plus courantes, est venu également tout un tas d'outils et démons fournis avec le paquet original de systemd. Certains sont incontournables, comme Journald.

De manière `tail -f`, `journalctl -f` continue sur la console jusqu'à "ctrl+c"
Consulter le journal depuis hier `journalctl --since yesterday`

> https://www.linuxtricks.fr/wiki/utiliser-journalctl-les-logs-de-systemd

## La gestion de processus et l'exécution des programmes

### Lister les process

- `ps -aux`

```sh
->ps -aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.3 136248  6448 ?        Ss   oct.21   0:15 /usr/lib/systemd/systemd --system --deserialize 15
root         2  0.0  0.0      0     0 ?        S    oct.21   0:00 [kthreadd]
root         4  0.0  0.0      0     0 ?        S<   oct.21   0:00 [kworker/0:0H]
```

Cumulé avec un grep permet de faire des recherches performantes

```sh
->ps -aux | grep apache
apache   28837  0.0  0.4 430488  9320 ?        S    13:47   0:00 /usr/sbin/httpd -DFOREGROUND
apache   28838  0.0  0.3 430224  6956 ?        S    13:47   0:00 /usr/sbin/httpd -DFOREGROUND
```

### Les process zombies

Les états (colonne `STAT`) les plus connus sont l'état R (en cours d’exécution), S (en sommeil), T (stoppé) ou encore Z (zombie). Ce dernier est particulier, car il désigne un processus qui, bien qu'ayant terminé son exécution, reste présent sur le système, en attente d'être pris en compte par son père.

- `ps -aux | grep Z` ou `ps -aux | grep defunct`

![Zombie](./resources/Capture%20d’écran%20de%202019-10-22%2015-50-07.png "Zombie")

> site amusants sur le sujet :
> https://www.it-connect.fr/les-processus-zombies/

## Monitoring

- `top` ou `htop` Linux `Top` command is a performance monitoring program and `Htop` is a much advanced interactive and real time Linux process monitoring tool.
- `lsof` List Open Files
- `vmstat` Virtual Memory Statistics
- `netstat` Network Statistics
- `nmon` Monitor Linux Performance -> installation sur centos 7 https://gist.github.com/sebkouba/f2a982ea1c2b658574dcc3da8de09de6

![nmon](./resources/Capture%20d’écran%202019-10-22%20à%2022.06.34.png "nmon")


> et plein d'autres https://www.tecmint.com/command-line-tools-to-monitor-linux-performance/

## Installation d'un Wordpress

Contexte je suis client "frileux" et je veux un wordpress

https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-on-centos-7

Pré-requis :
https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-centos-7

ou

https://www.linuxtricks.fr/wiki/installer-et-configurer-lamp-sur-centos-7

### LAMP

#### installation httpd

- `sudo yum httpd`
- `sudo systemctl enable httpd.service` activer le server http au démarrage
- `sudo systemctl start httpd.service` demarre le server http
- `sudo systemctl status httpd.service` vérifier le bon démarrage du serveur http
- `netstat`, pour "network statistics", est une ligne de commande affichant des informations sur les connexions réseau.

```sh
$ netstat -an | grep LISTEN
-bash: netstat : commande introuvable

$ sudo yum install net-tools
...
Installé :
net-tools.x86_64 0:2.0-0.25.20131004git.el7
...

$ netstat -an | grep LISTEN
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN
tcp6       0      0 ::1:25                  :::*                    LISTEN
tcp6       0      0 :::80                   :::*                    LISTEN
tcp6       0      0 :::22                   :::*                    LISTEN

$ curl localhost:80
```

- pare feu système

```sh
$ sudo firewall-cmd --permanent --zone=public --add-service=http
// pas obligatoire mais si on veut accepter le 443 -> --add-service=https
$ sudo firewall-cmd --reload
```

Port par défaut du protocol http en non securisé `80` 
Port par défaut du protocol http securisé (https) `443` 

#### VirtualHost

- `/etc/httpd` : dossier contenant l'ensemble des fichiers de configuration
  - `/etc/httpd/conf/httpd.conf` : fichier principal de configuration. Nous verrons qu'il est possible - et fortement recommandé - de ne jamais modifier ce fichier
  - `/etc/httpd/conf.d` : dossier contenant les fichiers secondaires de configuration, fournis par les extensions (apache 2.2) et les logiciels utilisant Apache.

> Remarque : pour configurer plusieurs sites DNS pour la meme adresse IP, on peut gratuitement utilisé `xip.io` -> https://github.com/basecamp/xip-pdns

*Configuration complexe* donc on est resté sur la conf de base -> `/var/www/html` pour le DirectoryRoot de notre application

> Pour aller plus loin, une bonne documentation de cas pratiques en plus de celle officielle https://www.microlinux.fr/apache-centos-7/


**Modifier la config d'un fichier pensez à horodater le fichier**

`cp -p monfichier.conf monfichier.conf.$(date +%Y%m%d)`

```sh
-p                           identique à --preserve=mode,ownership,timestamps
    --préserve[=ATTR_LIST]   préserver les attributs indiqués (par défaut
                               « mode,ownership,timestamps ») et si possible
                               les attributs supplémentaires « context »,
                               « links », « xattr » et « all »
```

```sh
$ ls -la
total 8
drwxrwxr-x. 2 zenika zenika  61 24 oct.  11:52 .
drwxrwxrwt. 9 root   root   252 24 oct.  11:52 ..
-rw-rw-r--. 1 zenika zenika   4 24 oct.  11:52 monfichier.conf
-rw-rw-r--. 1 zenika zenika   4 24 oct.  11:52 monfichier.conf.20191024
```

### Consulter les logs

- `tail -f`
- `less` cumuler avec un `shift+f` permet de mettre less en attente d'ajout de data au fichier
![less](./resources/Capture%20d’écran%202019-10-24%20à%2012.08.47.png "less")  
Interrupt avec un `ctrl+c`

- **`/var/log`** ce répertoire est très important la plupart des applications et services logs dans ce répertoire 
on y retrouvera donc les logs d'accès et d'erreurs httpd `access_log` et  `error_log`

```sh
# ls /var/log/httpd/
access_log  error_log
```

#### Installation mariadb

- `mariadb-server`

> Name : mariadb-server
> Description : MariaDB is a multi-user, multi-threaded SQL database server. MariaDB is a community developed branch of MySQL.

- `mariadb`
 > Name : mariadb
 > Description : contains the standard MariaDB/MySQL client programs and generic MySQL files.

```sh
$ sudo yum install mariadb-server mariadb
$ sudo systemctl start mariadb.service
$ sudo systemctl enable mariadb.service
->netstat -an | grep LISTEN
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:3306            0.0.0.0:*               LISTEN
...
```

Le port d'écoute par défaut du serveur Mariadb/Mysql est `3306`

Il est important de réaliser des "dumps" de la base avant toute modification, en effet en production et/ou en dev vous serez ammené à  executer  des scripts
Une  sauvegarde avant "ne mange pas de pain":  
https://www.memoinfo.fr/tutoriels-linux/guide-sauvegarde-restauration-mysql/

```sh
//  realiser une  sauvegarde de la base de données wordpress
# mysqldump --user=root --password --databases wordpress > fichier_destination.sql

# cat fichier_destination.sql
-- MariaDB dump 10.17  Distrib 10.4.8-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: wordpress
-- ------------------------------------------------------
-- Server version	10.4.8-MariaDB
...
-- Dump completed on 2019-10-24 16:17:48

// Restauration dela base à l'aide du  fichier "dump"
# mysql --user=root --password < fichier_destination.sql
```

#### php


```sh
$ sudo yum install php php-mysql
// php-mysql est le connecteur php qui permet de dialoguer avec une bdd MariaDB/MySQL
```

- Vérifier que php fonctionne -> php info
- Création du fichier `/var/www/html/info.php`

Vérifier en accédant à la page suivante:
`http://VotreAdressePublique/info.php`

![php](./resources/default_php.png "php")  


> Remarque : il s'agit de PHP 5.4, so old !!!!

#### PHP 5.4 incompatible avec wordpress

> Extrait de https://fr.wordpress.org/about/requirements/ 
Note : si vous êtes dans un environnement hérité où vous avez uniquement des anciennes versions de PHP ou MySQL, sachez que WordPress fonctionne aussi avec PHP 5.6.20+ et MySQL 5.0+, mais ces versions sont en fin de vie et peuvent exposer votre site à des failles de sécurité.

Nous avons suivi ce tuto pour mettre à jour PHP, il nous fait installer le repo de la famille Collet: 
https://tecadmin.net/install-php7-on-centos7/

> https://wiki.centos.org/RemiCollet
> RPM Package Manager (RPM) is a free and open-source package management system for installing, uninstalling and managing software packages in Linux.


## FileSystem

Contexte : le client veut s'assurer que la base de données peut atteindre 50 GB or nous avons seulement mis 32GB a notre VM Centos


### LVM


![lvm](./resources/img_565adbb08ee77.png "lvm")  


https://www.linuxtricks.fr/wiki/lvm-sous-linux-volumes-logiques 
https://www.it-connect.fr/gestion-des-lvm-sous-linux/ 
https://blog.microlinux.fr/lvm-centos/ 
https://doc.ubuntu-fr.org/lvm 

> On retrouve souvent les mêmes sites comme référence

### Redimensionnement de la partition principale `centos` `root`

> TP: https://nopistash.wordpress.com/2017/06/01/proxmox-resize-linux-guest-partition/

```sh

[root@centos-01 httpd]# df -h
Sys. de fichiers        Taille Utilisé Dispo Uti% Monté sur
devtmpfs                  908M       0  908M   0% /dev
tmpfs                     919M       0  919M   0% /dev/shm
tmpfs                     919M    8,5M  911M   1% /run
tmpfs                     919M       0  919M   0% /sys/fs/cgroup
/dev/mapper/centos-root   119G    1,9G  118G   2% /
/dev/sda1                1014M    193M  822M  19% /boot
tmpfs                     184M       0  184M   0% /run/user/1000
[root@centos-01 httpd]# fdisk -l

Disque /dev/sda : 131.0 Go, 130996502528 octets, 255852544 secteurs
Unités = secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : dos
Identifiant de disque : 0x000cfece

Périphérique Amorçage  Début         Fin      Blocs    Id. Système
/dev/sda1   *        2048     2099199     1048576   83  Linux
/dev/sda2         2099200   255852543   126876672   8e  Linux LVM

Disque /dev/mapper/centos-root : 127.8 Go, 127771082752 octets, 249552896 secteurs
Unités = secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets


Disque /dev/mapper/centos-swap : 2147 Mo, 2147483648 octets, 4194304 secteurs
Unités = secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets

[root@centos-01 httpd]# pvdisplay
 --- Physical volume ---
 PV Name               /dev/sda2
 VG Name               centos
 PV Size               <121,00 GiB / not usable 2,00 MiB
 Allocatable           yes (but full)
 PE Size               4,00 MiB
 Total PE              30975
 Free PE               0
 Allocated PE          30975
 PV UUID               D0ex48-cfdp-YL34-ENf9-Omtp-16hF-VkN2kY

[root@centos-01 httpd]# vgdisplay
 --- Volume group ---
 VG Name               centos
 System ID
 Format                lvm2
 Metadata Areas        1
 Metadata Sequence No  5
 VG Access             read/write
 VG Status             resizable
 MAX LV                0
 Cur LV                2
 Open LV               2
 Max PV                0
 Cur PV                1
 Act PV                1
 VG Size               <121,00 GiB
 PE Size               4,00 MiB
 Total PE              30975
 Alloc PE / Size       30975 / <121,00 GiB
 Free  PE / Size       0 / 0
 VG UUID               Qx7d4m-GXmL-27PW-U4Cw-S6aO-F9k6-2nngoY

[root@centos-01 httpd]# lvdisplay
 --- Logical volume ---
 LV Path                /dev/centos/swap
 LV Name                swap
 VG Name                centos
 LV UUID                3fGOXB-Ey2h-T1vD-S0yO-vZCp-m5ab-utyfZe
 LV Write Access        read/write
 LV Creation host, time localhost.localdomain, 2019-10-21 10:54:47 +0200
 LV Status              available
 # open                 2
 LV Size                2,00 GiB
 Current LE             512
 Segments               1
 Allocation             inherit
 Read ahead sectors     auto
 - currently set to     8192
 Block device           253:1

 --- Logical volume ---
 LV Path                /dev/centos/root
 LV Name                root
 VG Name                centos
 LV UUID                bF3hr9-Hhu2-p4Gk-F6tO-Iwz6-E2hk-cN934a
 LV Write Access        read/write
 LV Creation host, time localhost.localdomain, 2019-10-21 10:54:47 +0200
 LV Status              available
 # open                 1
 LV Size                <119,00 GiB
 Current LE             30463
 Segments               1
 Allocation             inherit
 Read ahead sectors     auto
 - currently set to     8192
 Block device           253:0

```

Quelques commandes utiles:

```sh
[root@centos-01 httpd]# lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0  122G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0  121G  0 part
 ├─centos-root 253:0    0  119G  0 lvm  /
 └─centos-swap 253:1    0    2G  0 lvm  [SWAP]
sr0              11:0    1  552M  0 rom
[root@centos-01 httpd]# blkid
/dev/mapper/centos-root: UUID="6126aa7b-ef34-4934-b8ba-67975ff96bb6" TYPE="xfs"
/dev/sda2: UUID="D0ex48-cfdp-YL34-ENf9-Omtp-16hF-VkN2kY" TYPE="LVM2_member"
/dev/sr0: UUID="2019-09-06-12-49-13-00" LABEL="CentOS 7 x86_64" TYPE="iso9660" PTTYPE="dos"
/dev/sda1: UUID="6201fd8e-81cf-4c16-a662-15cbfb50a523" TYPE="xfs"
/dev/mapper/centos-swap: UUID="f4b8c888-289b-4bff-b128-449b70118bef" TYPE="swap"
```


### `mount` et `umount`

L’action qui consiste à rendre une unité de stockage accessible s’appelle le montage. Elle est réalisée par la commande "mount". Le montage utilise un répertoire déjà existant et y crée un point de montage.
L'opération inverse, le démontage, supprime le point de montage, ce qui rend inaccessible l'unité de stockage / partition et rend de nouveau accessible le contenu du répertoire que le montage avait masqué. Cette opération est effectuée par la commande "umount".

Liens utiles: 
https://doc.ubuntu-fr.org/montage 
https://doc.ubuntu-fr.org/mount_fstab

> Quelques explications sur la commande `mount` https://artisan.karma-lab.net/comprendre-montages-sous-linux



## Notions de proxy / reverse proxy et load balancer

https://fr.wikipedia.org/wiki/Proxy_inverse

![proxy](./resources/main-qimg-f15380cda460abbee5f91536c67a5d8e-c.jpeg "proxy")

Load Balancer : répartiteur de charge

![lb](./resources/loadbalancer.png "lb")


> Attention à bien choisir son proxy, certains proxys n'interviennent pas dans les mêmes couches 
![osi](./resources/TCPIP_couche_ISO_modele_OSI.png "osi")   
http://blog.metrink.com/blog/2014/08/12/httpd-vs-nginx-vs-haproxy/



## Mettre en place un cluster Galera

> /!\ le repo centos contient vraiment des paquets plus qu'obsolétes, la version de MariaDB installée est trop ancienne, nous avons besoin d'une version 10 a minima

```sh
# cat /var/log/messages | grep mariadb
Oct 21 12:05:11 centos-01 yum[15475]: Installed: 1:mariadb-5.5.64-1.el7.x86_64
Oct 21 12:05:27 centos-01 yum[15475]: Installed: 1:mariadb-server-5.5.64-1.el7.x86_64
```



https://www.digitalocean.com/community/tutorials/how-to-configure-a-galera-cluster-with-mariadb-on-centos-7-servers

La procédure de yum install` de la dernière version du repo officiel de mariadb à automatiquement réalisé un update.

```sh
# cat /var/log/messages | grep mari -C 5 // -C comme context avec 5 lignes avant et après
...
Oct 23 16:49:54 centos-01 dbus[670]: [system] Reloaded configuration
Oct 23 16:49:55 centos-01 yum[10319]: Installed: MariaDB-server-10.4.8-1.el7.centos.x86_64
Oct 23 16:49:55 centos-01 yum[10319]: Erased: 1:mariadb-server-5.5.64-1.el7.x86_64
Oct 23 16:49:55 centos-01 systemd: Reloading.
Oct 23 16:49:55 centos-01 yum[10319]: Erased: 1:mariadb-5.5.64-1.el7.x86_64
Oct 23 16:50:00 centos-01 yum[10319]: Erased: 1:mariadb-libs-5.5.64-1.el7.x86_64
````

```sh
# mysql -u root -p
MariaDB [(none)]> SHOW VARIABLES LIKE "%version%";
+-----------------------------------+------------------------------------------+
| Variable_name                     | Value                                    |
+-----------------------------------+------------------------------------------+
| in_predicate_conversion_threshold | 1000                                     |
| innodb_version                    | 10.4.8                                   |
| protocol_version                  | 10                                       |
| slave_type_conversions            |                                          |
| system_versioning_alter_history   | ERROR                                    |
| system_versioning_asof            | DEFAULT                                  |
| tls_version                       | TLSv1.1,TLSv1.2,TLSv1.3                  |
| version                           | 10.4.8-MariaDB                           |
| version_comment                   | MariaDB Server                           |
| version_compile_machine           | x86_64                                   |
| version_compile_os                | Linux                                    |
| version_malloc_library            | system                                   |
| version_source_revision           | 4c2464b87d58a43d1292e166bae6720b51f4b000 |
| version_ssl_library               | OpenSSL 1.0.2k-fips  26 Jan 2017         |
| wsrep_patch_version               | wsrep_26.22                              |
+-----------------------------------+------------------------------------------+
15 rows in set (0.001 sec)
```

La bonne surprise est que la MAJ de MariaDB n'a pas impacté la base de données de wordpress, il y a bien eu retrocompatibilité.

Une fois le paramétrage Galera correctement réalisé, vous pouvez rencontrer le problème suivant, le cluster dispose bien du  bon nombre de noeud la BDD se synchronise, les tables sont bien présentes.
Mais le contenu des tables est vide. Un rapide coup d'oeil sur l'internet donne l'information suivante : 
https://dba.stackexchange.com/questions/97146/mariadb-galera-cluster-master-master-does-sync-tables-but-no-data

Dans le tuto, il est ecrit "For example, Galera won’t work with MyISAM". Un autre coup d'oeil sur internet permet de confirmer la problématique. 
https://setra-conseil.com/blog/myisam-vs-innodb/ 
https://mariadb.com/kb/en/library/mariadb-galera-cluster-known-limitations/


Une modification de la table `ALTER TABLE equipment ENGINE=InnoDB, FORCE;` me permet de récupérer les données :)

```sh
MariaDB [playground]> SHOW TABLE STATUS WHERE Name='equipment'\G;
*************************** 1. row ***************************
           Name: equipment
         Engine: InnoDB
        Version: 10
     Row_format: Dynamic
           Rows: 3
 Avg_row_length: 5461
    Data_length: 16384
Max_data_length: 0
   Index_length: 0
      Data_free: 0
 Auto_increment: 4
    Create_time: 2019-10-24 15:33:18
    Update_time: 2019-10-24 15:34:39
     Check_time: NULL
      Collation: latin1_swedish_ci
       Checksum: NULL
 Create_options:
        Comment:
Max_index_length: 0
      Temporary: N
1 row in set (0.000 sec)

```


```sh
MariaDB [(none)]> SELECT * FROM playground.equipment;
+----+--------+-------+--------+
| id | type   | quant | color  |
+----+--------+-------+--------+
|  1 | slide  |     2 | blue   |
|  2 | swing  |    10 | yellow |
|  3 | seesaw |     3 | green  |
+----+--------+-------+--------+
3 rows in set (0.001 sec)
```

# Astuces de la fin

## Script Bash

http://wiki.linux-france.org/wiki/Les_commandes_fondamentales_de_Linux/Programmation_bash_(script)

`set -xe` et/ou `set -o verbose` permet d'afficher chaque commande lors de son exécution.

## Les Cheat Sheet du SysAdmin

**Ultimate list of Cheat Sheet for System Administrator**
https://geekflare.com/cheat-sheet-system-admin/ 

Mes préférences:
http://cb.vu/unixtoolbox.xhtml
https://www.di.ens.fr/~pouzet/cours/systeme/unix/admin_Linux.html 

## Un conseil

*Lire les logs et isoler ce qui fonctionne de ce qui ne fonctionne pas*


